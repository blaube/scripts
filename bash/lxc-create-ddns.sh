#!/bin/bash
# Created by Brian Laube
# Description: Create a LXC container with DDNS

if [[ -z "$1" ]]; then
	echo "Provide container name"
	exit
fi
vm_name=$1
vm_user="ubuntu"
vm_group="ubuntu"

lxc init images:ubuntu/21.10/amd64 ${vm_name}
lxc profile add ${vm_name} vlanXXX
lxc start ${vm_name}

echo "Waiting for network..."
while [[ -z "$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)" ]] ; do
	sleep 1
done
vm_ip=$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)

lxc exec ${vm_name} -- timedatectl set-timezone America/New_York
lxc exec ${vm_name} -- apt update
lxc exec ${vm_name} -- apt install -y wget nano ddclient
echo "Change password for user 'root'"
lxc exec ${vm_name} -- passwd root
echo "Change password for user 'ubuntu'"
lxc exec ${vm_name} -- passwd ubuntu
