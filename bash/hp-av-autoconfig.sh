#!/bin/bash
# Created by Brian Laube 2020
# Description: Set Display & Audio for HP Thin Clients

# Computer Type
h=$(hostname)
client_type=${h:3:2}

# Current Audio Device
current_audio=$(sudo -u user pacmd info | grep "Default sink name" | awk '{print $4}')

# Display Information
dp0_status=$(xrandr | grep -i DisplayPort-0 | awk '{print $2}')
dp0_res=$(xrandr | grep -i DisplayPort-0 | awk '{print $3}')
dp1_status=$(xrandr | grep -i DisplayPort-1 | awk '{print $2}')
dp1_res=$(xrandr | grep -i DisplayPort-1 | awk '{print $3}')
dp1_audio=$(sudo -u user pacmd list-sinks | grep -i name: | grep -i hdmi | awk '{print $2}' | sed 's/<//g;s/>//g')
dp1_audio_index=$(sudo -u user pacmd list-sinks | grep -i name: -B1 | grep -i hdmi -B1 | head -n1 | awk '{print $3}' | sed 's/<//g;s/>//g')

# Functions
function_mirror () {
	echo "	Checking: DP0"
	if [[ $dp0_status == "connected" ]]; then
		if [[ $dp0_res != "1920x1080+0+0" ]]; then
			echo "DP0 reconfigured"
			xrandr --output DisplayPort-0 --mode 1920x1080 --pos 0x0
		fi
	fi
	echo "	Checking: DP1"
	if [[ $dp1_status == "connected" ]]; then
		if [[ $dp1_res != "1920x1080+0+0" ]]; then
			echo "DP1 reconfigured"
			xrandr --output DisplayPort-1 --mode 1920x1080 --pos 0x0
		fi
	fi
}

function_extend_1080 () {
	echo "	Checking: DP0"
	if [[ $dp0_status == "connected" ]]; then
		if [[ $dp0_res != "1920x1080+0+0" ]]; then
			echo "DP0 reconfigured"
			xrandr --output DisplayPort-0 --mode 1920x1080 --pos 0x0
		fi
	fi
	echo "	Checking: DP1"
	if [[ $dp1_status == "connected" ]]; then
		if [[ $dp1_res != "1920x1080+1920+0" ]]; then
			echo "DP1 reconfigured"
			xrandr --output DisplayPort-1 --mode 1920x1080 --pos 1920x0
		fi
	fi
}

function_extend_4k () {
	echo "	Checking: DP0"
	if [[ $dp0_status == "connected" ]]; then
		if [[ $dp0_res != "1920x1080+0+0" ]]; then
			echo "DP0 reconfigured"
			xrandr --output DisplayPort-0 --mode 1920x1080 --pos 0x0
		fi
	fi
	echo "	Checking: DP1"
	if [[ $dp1_status == "connected" ]]; then
		if [[ $dp1_res != "3840x2160+1920+0" ]]; then
			echo "DP1 reconfigured"
			xrandr --output DisplayPort-1 --mode 3840x2160 --pos 1920x0
		fi
	fi
}

function_set_audio () {
	echo "	Checking audio device"
	if [[ $current_audio != $dp1_audio ]]; then
		echo "	Setting audio device to $dp1_audio"
		sudo -u user pacmd set-default-sink $dp1_audio
	fi
	sudo -u user pacmd set-sink-volume $dp1_audio_index 65536
	sudo -u user pacmd set-sink-mute $dp1_audio_index 0
}

# Main Script
echo -e "\n$client_type DETECTED\n	DP0: $dp0_status $dp0_res\n	DP1: $dp1_status $dp1_res"
case $client_type in

	"IT")
		function_mirror
		function_set_audio
	;;

	"AT")
		function_extend_1080
	;;

	"BT")
		function_extend_1080
	;;

	"TT")
		function_mirror
		function_set_audio
	;;

	*)
		echo "	INCORRECT HOSTNAME: $(hostname)"
	;;

esac

echo -e "DONE\n"
