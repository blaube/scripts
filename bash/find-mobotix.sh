#!/bin/bash
# Created by Brian Laube 2020
# Description: Find Mobotix cameras on a network

for i in `nmap -sn $1 | grep -i report | awk '{ print $5 }'`
do
   echo $i
   echo "" | nc -n -w1 $i 80 | egrep -i "\-MX"
done