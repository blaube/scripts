#!/bin/bash
# Created by Brian Laube
# Description: Create a LXC container with Plex Media Server

if [[ -z "$1" ]]; then
        echo "Provide container name"
        exit
fi
vm_name=$1
vm_user="ubuntu"
vm_group="ubuntu"
plex_install="https://downloads.plex.tv/plex-media-server-new/1.25.2.5319-c43dc0277/debian/plexmediaserver_1.25.2.5319-c43dc0277_amd64.deb"
plex_file=$(echo ${plex_install} | awk -F'/' '{print $7}')

lxc init images:ubuntu/21.10/amd64 ${vm_name}
lxc profile add ${vm_name} vlanXXX
lxc config set ${vm_name} raw.idmap "both 1000 1000"
lxc config device add ${vm_name} media disk source=/home/user/Videos/ path=/mnt/Videos/
lxc start ${vm_name}

echo "Waiting for network..."
while [[ -z "$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)" ]] ; do
	sleep 1
done
vm_ip=$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)

lxc exec ${vm_name} -- timedatectl set-timezone America/New_York
lxc exec ${vm_name} -- apt update
lxc exec ${vm_name} -- apt install -y wget ssh
lxc exec ${vm_name} -- wget ${plex_install}
lxc exec ${vm_name} -- apt install -y /root/${plex_file}
lxc exec ${vm_name} -- usermod -aG ${vm_group} plex
lxc exec ${vm_name} -- usermod -aG plex ${vm_user}
echo "Change password for user 'root'"
lxc exec ${vm_name} -- passwd root
echo "Change password for user 'ubuntu'"
lxc exec ${vm_name} -- passwd ubuntu
echo -e "${vm_ip}:32400/web\nUse SSH tunnel to access the webui for initial setup."
