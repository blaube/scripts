#!/bin/bash
# Created by Brian Laube 2021
# Description: Use to initiate reverse SSH tunnel via cron

open=`ps aux | grep -i "tmux new -d reverse_ssh_tunnel.sh" | grep -v grep | wc -l`

if [[ $open -eq 0 ]]; then
	echo "Tunnel not running. Starting..."
	tmux new -d  "reverse_ssh_tunnel.sh"
else
	echo "Tunnel running. Do nothing."
fi
