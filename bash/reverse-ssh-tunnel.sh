#!/bin/bash
# Created by Brian Laube 2021
# Description: Create a persistent reverse SSH tunnel for access to device on remote network

while :
do
	echo "`date` Checking for network..."
	ping -c1 -s1 -w1 8.8.8.8 > /dev/null 2>&1
	if [ $? -eq 0 ] ; then
		echo "`date` Connecting... "
		ssh user@sub.domain.com -o ServerAliveInterval=10 -o ServerAliveCountMax=1 -R9999:localhost:22 -N
		echo "`date` Disconnected."
		sleep 5
	else
		echo "`date` Ping failed. Killing previous SSH sessions..."
		killall -9 ssh
		echo "`date` Resetting Network Adapter..."
		sudo ip link set eth0 down && sudo ip link set eth0 up
	        sleep 60
	fi
done
