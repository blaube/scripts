#!/bin/bash
# Created by Brian Laube
# Description: Create a LXC container with Deluge

if [[ -z "$1" ]]; then
        echo "Provide container name"
        exit
fi
vm_name=$1
vm_user="ubuntu"
vm_group="ubuntu"

lxc init images:ubuntu/21.10/amd64 ${vm_name}
lxc profile add ${vm_name} vlanXXX
lxc config set ${vm_name} raw.idmap "both 1000 1000"
lxc config device add ${vm_name} downloads disk source=/home/user/Downloads/ path=/mnt/Downloads/
lxc start ${vm_name}

echo "Waiting for network..."
while [[ -z "$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)" ]] ; do
	sleep 1
done
vm_ip=$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)

lxc exec ${vm_name} -- timedatectl set-timezone America/New_York
lxc exec ${vm_name} -- apt update
lxc exec ${vm_name} -- apt install -y deluged deluge-web nano
lxc exec ${vm_name} -- systemctl stop deluged.service
lxc exec ${vm_name} -- systemctl disable deluged.service
lxc exec ${vm_name} -- rm /run/systemd/generator.late/deluged.service
lxc exec ${vm_name} -- bash -c "echo -e '[Unit]\nDescription=Deluge Bittorrent Client Daemon\nAfter=network-online.target\n\n[Service]\nType=simple\nUser=${vm_user}\nGroup=${vm_group}\nUMask=007\nExecStart=/usr/bin/deluged -d\nRestart=on-failure\nTimeoutStopSec=300\n\n[Install]\nWantedBy=multi-user.target' > /etc/systemd/system/deluged.service"
lxc exec ${vm_name} -- bash -c "echo -e '[Unit]\nDescription=Deluge Bittorrent Client Web Interface\nAfter=network-online.target\n\n[Service]\nType=simple\nUser=ubuntu\nGroup=ubuntu\nUMask=027\nExecStart=/usr/bin/deluge-web -d\nRestart=on-failure\n\n[Install]\nWantedBy=multi-user.target' > /etc/systemd/system/deluge-web.service"
lxc exec ${vm_name} -- systemctl daemon-reload
lxc exec ${vm_name} -- systemctl start deluged deluge-web
lxc exec ${vm_name} -- systemctl enable deluged deluge-web
echo "Change password for user 'root'"
lxc exec ${vm_name} -- passwd root
echo "Change password for user 'ubuntu'"
lxc exec ${vm_name} -- passwd ubuntu
echo -e "${vm_ip}:8112"
