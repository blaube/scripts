#!/bin/bash

while getopts "f::c::" opt; do
    case $opt in
        f)
            front=${OPTARG}
            if (( front < 0 )); then front=0; fi
            if (( front > 255 )); then front=255; fi
            echo "Front: $front"
            echo "$front" > /sys/class/hwmon/hwmon1/pwm1
            ;;
        c)
            cpu=${OPTARG}
            if (( cpu < 0 )); then cpu=0; fi
            if (( cpu > 255 )); then cpu=255; fi
            echo "CPU: $cpu"
            echo "$cpu" > /sys/class/hwmon/hwmon1/pwm2
            ;;
        *)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
    esac
done