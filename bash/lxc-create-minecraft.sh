#!/bin/bash
# Created by Brian Laube
# Description: Create a LXC container with Minecraft Bedrock Server

if [[ -z "$1" ]]; then
	echo "Provide container name"
	exit
fi
vm_name=$1
vm_user="ubuntu"
vm_group="ubuntu"
# GET DOWNLOAD URL FROM https://www.minecraft.net/en-us/download/server/bedrock
install_url="https://minecraft.azureedge.net/bin-linux/bedrock-server-1.17.41.01.zip"
install_filename=$(echo ${install_url} | awk -F'/' '{print $5}')

lxc init images:ubuntu/21.10/amd64 ${vm_name}
lxc profile add ${vm_name} vlanXXX
lxc start ${vm_name}

echo "Waiting for network..."
while [[ -z "$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)" ]] ; do
	sleep 1
done
vm_ip=$(lxc exec ${vm_name} -- ip a show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)

lxc exec ${vm_name} -- timedatectl set-timezone America/New_York
lxc exec ${vm_name} -- apt update
lxc exec ${vm_name} -- apt install -y wget unzip nano tmux libcurl4-openssl-dev
lxc exec ${vm_name} -- wget ${install_url} -P /home/ubuntu/
lxc exec ${vm_name} -- unzip /home/ubuntu/${install_filename} -d /home/ubuntu/
lxc exec ${vm_name} -- bash -c "echo 'cd /home/ubuntu/' > /usr/local/bin/minecraft"
lxc exec ${vm_name} -- bash -c "echo 'tmux new-session \"LD_LIBRARY_PATH=. ./bedrock_server\"' >> /usr/local/bin/minecraft"
lxc exec ${vm_name} -- chown -R ubuntu: /home/ubuntu/
lxc exec ${vm_name} -- chown -R ubuntu: /usr/local/bin/minecraft
lxc exec ${vm_name} -- chmod +x /usr/local/bin/minecraft
lxc exec ${vm_name} -- chmod +x /home/ubuntu/bedrock_server
echo "Change password for user 'root'"
lxc exec ${vm_name} -- passwd root
echo "Change password for user 'ubuntu'"
lxc exec ${vm_name} -- passwd ubuntu
echo "lxc exec ${vm_name} -- sudo -u ubuntu --login minecraft"
