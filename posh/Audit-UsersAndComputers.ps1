# Created by Brian Laube 2017
# Description: Check if user or computer accounts have been used within a specific timeframe.
# If "days" is specified as a negative number, search range includes dates on and before the specified number of days ago.
# If "days" is specified as a positive number, search range includes dates on and after the specified number of days ago.

param (
    [Parameter(Mandatory=$true)][string]$Type,
    [Parameter(Mandatory=$false)][string]$Enabled,
    [Parameter(Mandatory=$false)][int]$Days = -270
)

Import-Module ActiveDirectory

$DirDate = Get-Date -format yyyyMMddTHHmm
$path = Get-Location
$scriptName = $MyInvocation.MyCommand.Name
$outFile = "$path\export\$scriptName-$DirDate.csv"

if ([int]$Days -lt 0){
    $Date = get-date -date $(get-date).AddDays($Days) -format MM-dd-yyyy
    $customFilter = { LastLogonDate -le $Date }
    switch ($Enabled){
        {@("t","true") -contains $_}{
            $customFilter = { LastLogonDate -le $Date -and Enabled -eq $true }
        }
        {@("f","false") -contains $_}{
            $customFilter = { LastLogonDate -le $Date -and Enabled -eq $false }
        }
    }
    Write-Output " * Searching Dates ON and BEFORE: $Date"
} else {
    $Date = get-date -date $(get-date).AddDays(-$Days) -format MM-dd-yyyy
    $customFilter = { LastLogonDate -ge $Date }
    switch ($Enabled){
        {@("t","true") -contains $_}{
            $customFilter = { LastLogonDate -ge $Date -and Enabled -eq $true }
        }
        {@("f","false") -contains $_}{
            $customFilter = { LastLogonDate -ge $Date -and Enabled -eq $false }
        }
    }
    Write-Output " * Searching Date range: $Date through $(get-date -format MM-dd-yyyy)"
}

switch ($type){
    {@("u","user","users") -contains $_ }{
        $dataOut = Get-ADUser -Properties * -Filter $customFilter |
         Select-Object Name,SAMaccountname,LogonCount,LastLogonDate,Enabled
    }
    {@("c","computer","computers") -contains $_ }{
        $dataOut = Get-ADComputer -Properties * -Filter $customFilter |
         Select-Object Name,LastLogonDate
    }
}

$dataOut | Sort-Object LastLogonDate | Export-Csv -Path $outFile -NoTypeInformation