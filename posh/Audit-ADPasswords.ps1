# Created by Brian Laube 2018
# Description: Export text file of users with weak passwords
# Requires DSInternals Module to be installed
# PS> Install-Module -Name DSInternals
# https://www.dsinternals.com/

param (
    [Parameter(Mandatory=$false)][string]$Dictionary,
    [Parameter(Mandatory=$false)][string]$Password
)

Import-Module DSInternals

$DirDate = Get-Date -format yyyyMMddTHHmm
$path = Get-Location
$scriptName = $MyInvocation.MyCommand.Name
$outFile = "$path\$scriptName-$DirDate.txt"

if ($Password){
    $dict = $Password
} else {
    $dict = Get-Content $Dictionary
}

$scan = Get-ADReplAccount -All -Server "DC" -NamingContext "dc=domain,dc=com" |
Test-PasswordQuality -WeakPasswords $dict -IncludeDisabledAccount

$scan.WeakPassword | Out-File -FilePath $outFile
