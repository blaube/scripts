# Created by Brian Laube 2018

Import-Module ActiveDirectory

$DirDate = Get-Date -format yyyyMMddTHHmm
$path = Get-Location
$scriptName = $MyInvocation.MyCommand.Name
$outFile = "$path\export\$scriptName-$DirDate.csv"

$dataOut = Get-ADUser -Filter "*" -searchbase "OU=users,DC=domain,DC=local" -Properties * | Select-Object SAMaccountname,DisplayName,GivenName,SN,emailaddress
$dataOut | Sort-Object SAMaccountname | Export-Csv -Path $outFile -NoTypeInformation