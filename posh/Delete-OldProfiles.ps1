# Created by Brian Laube 2024
# Description: Delete user profiles that have not been used in a specified number of days

param (
    [int] $Days,        # Number of days to go back. Use a negative number.
    $IgnoreAccounts,    # Case-sensitive. Format -IgnoreAccounts Account1, Account2, account3
    [switch]$Force      # Use -Force to perform destructive actions.
);

$ErrorActionPreference = "SilentlyContinue"
$Path = "C:\Users"
$UserFolders = $Path | Get-ChildItem -Directory

if ($Force -eq $False) {
    write-host ""
    write-host "LISTING PROFILES, NON-DESTRUCTIVE. USE -force TO PERFORM DESTRUCTIVE ACTIONS."
    write-host ""
}

ForEach ($UserFolder in $UserFolders) {
    $UserName = $UserFolder.Name
    if ($UserName.contains("Public")){
        continue
    }
    $ignore = 0
    ForEach ($Account in $IgnoreAccounts) {
        if ($UserName.contains($Account)){
            $ignore = 1
        }
    }

    if ($ignore -eq 0) {
        if (($UserFolder.LastWriteTime -lt $(Get-Date).AddDays($Days))) {
            $CI = Get-CimInstance Win32_UserProfile | Where-Object {$_.LocalPath -eq "$Path\$UserFolder" -and $_.Loaded -eq $False}
            if ($CI) {
                write-host -ForegroundColor red [REM] $UserName $UserFolder.LastWriteTime
                if ($Force -eq $True) {
                    $CI | Remove-CimInstance
                }
            } else {
                write-host -ForegroundColor yellow [FAIL] $UserName $UserFolder.LastWriteTime
            }
        } else {
            write-host -ForegroundColor white $UserName $UserFolder.LastWriteTime
        }
    } else {
        write-host -ForegroundColor cyan $UserName $UserFolder.LastWriteTime
    }
}