# Created by Brian Laube 2024
# Description: Start Disk Cleanup with the desired options


# Define the registry key where the Disk Cleanup settings will be stored
$RegKey = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\VolumeCaches"

# Define the list of Disk Cleanup options to enable (under registry path)
$CleanupOptions = @(
    "Delivery Optimization Files",
    "D3D Shader Cache",
    "Internet Cache Files",
    "Recycle Bin",
    "Temporary Files",
    "Thumbnail Cache",
    "System error memory dump files",
    "System error minidump files",
    "Windows Error Reporting Files",
    "Windows Upgrade Log Files"
)

function writelog([string]$message) {
    # Get the current date and time
    $datetime = Get-Date -Format "yyyyMMddTHHmmss"
    
    # Print the message to the console
    Write-Host "$datetime $message"
    
    # Append the message to a log file
    if(!(Test-Path -Path "$env:SystemDrive\scriptlogs")) {
        New-Item -ItemType Directory -Force -Path "$env:SystemDrive\scriptlogs"
    }
    $logFileName = (Split-Path $MyInvocation.PSCommandPath -Leaf) -replace '.ps1$', '.txt'
    "$datetime $message" | Out-File -Append "$env:SystemDrive\scriptlogs\$logFileName"
}

function Remove-Folder($path) {
    if (Test-Path $path) {
        Get-ChildItem -Path $path -Recurse | Remove-Item -Force -Recurse -ErrorAction SilentlyContinue
    } else {
        Write-Output "Path $path does not exist."
    }
}

# Check if cleanmgr.exe is running
$process = Get-Process cleanmgr -ErrorAction SilentlyContinue
if ($process) {
    $process | Stop-Process -Force
    Start-Sleep -Seconds 3
    # writelog("cleanmgr.exe is running. Please close it and rerun this script.")
    # exit
}

# Enable the desired Disk Cleanup options in the registry
foreach ($Option in $CleanupOptions) {
    Set-ItemProperty -Path "$RegKey\$Option" -Name "StateFlags0001" -Value 2
    writelog("$RegKey\$Option")
}

# Run Disk Cleanup with the configured options
writelog("Start Disk Cleanup")
Start-Process -FilePath "cleanmgr.exe" -ArgumentList "/sagerun:1"

# Clear the Recycle Bin
writelog("Empty Recycle Bin")
Get-ChildItem -Path 'C:\$Recycle.Bin' -Force | Remove-Item -Recurse -ErrorAction SilentlyContinue

# Clear the Windows TEMP folder
writelog("$env:windir\TEMP")
Remove-Folder -path "$env:windir\TEMP"

# Since we're not running disk cleanup as each user, specify user folders to clean up
$userProfiles = Get-ChildItem "C:\Users\" -Directory
foreach ($userProfile in $userProfiles) {
    if ($userProfile.Name -eq "Public") {
        continue
    }

    $tempFilesPath = Join-Path $userProfile.FullName "AppData\Local\Temp"
    writelog("$tempFilesPath")
    Remove-Folder -path $tempFilesPath
}