# Created by Brian Laube 2024/02/23
# Description: Compare the hosts from multiple sources, generate a report which hosts are missing from each source

Import-Module ActiveDirectory

# Ignore specific hostnames for specific categories (TESTING)
$ignoreHosts = @{
    "ALL" = @(
        # "HOSTNAME",
        # "HOSTNAME2"
    )
    "AD" = @(
        # "HOSTNAME",
        # "HOSTNAME2"
    )
    # ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------
    "Adlumin" = @(
        # "HOSTNAME",
        # "HOSTNAME2"
    )
    # ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------
    "CrowdStrike" = @(
        # "HOSTNAME",
        # "HOSTNAME2"
    )
    "Nessus" = @(
        # "HOSTNAME",
        # "HOSTNAME2"
    )
}

# ------------------------------ AD HOSTS ------------------------------
$ad_hosts = Get-ADComputer -Filter * | Where-Object { $_.Enabled -eq $true } | ForEach-Object { $_.Name }

# ------------------------------ CROWDSTRIKE HOSTS ------------------------------
$crowdstrike_export = Import-Csv -Path 'Compare-Inventory/crowdstrike_export.csv'
$crowdstrike_hosts = $crowdstrike_export.'Hostname'

# ------------------------------ NESSUS HOSTS ------------------------------
$nessus_export = Import-Csv -Path 'Compare-Inventory/nessus_export.csv'
$nessus_hosts = $nessus_export.'Agent Name'

# ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------
# ------------------------------ ADLUMIN HOSTS ------------------------------
$adlumin_export = Import-Csv -Path 'Compare-Inventory/adlumin_export.csv'
$adlumin_hosts = $adlumin_export.'Hostname'
# ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------

# ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF SOPHOS ------------------------------
# ------------------------------ SOPHOS HOSTS ------------------------------
$sophos_export = Import-Csv -Path 'Compare-Inventory/sophos_export.csv'
$sophos_hosts = $sophos_export.'Name'
$av_hosts = $crowdstrike_hosts + $sophos_hosts | Sort-Object -Unique
$crowdstrike_hosts = $av_hosts # Hacky way to combine the AVs together while leaving it simple to remove this section
# ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF SOPHOS ------------------------------

# Combine all arrays into a hashtable for easy lookup
$allItems = @{}
foreach ($item in ($ad_hosts + $adlumin_hosts + $crowdstrike_hosts + $nessus_hosts)) {
    # ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------
    $allItems[$item] = @{"AD" = $false; "Adlumin" = $false; "CrowdStrike" = $false;  "Nessus" = $false}
    # ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------

    # ---------------------------- UNCOMMENT THIS SECTION WHEN WE GET RID OF ADLUMIN -----------------------------
    # $allItems[$item] = @{"AD" = $false; "CrowdStrike" = $false;  "Nessus" = $false}
    # ---------------------------- UNCOMMENT THIS SECTION WHEN WE GET RID OF ADLUMIN -----------------------------
}

# Mark the items present in each array
foreach ($item in $ad_hosts) { $allItems[$item]["AD"] = $true }
# ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------
foreach ($item in $adlumin_hosts) { $allItems[$item]["Adlumin"] = $true }
# ------------------------------ REMOVE THIS SECTION WHEN WE GET RID OF ADLUMIN ------------------------------
foreach ($item in $av_hosts) { $allItems[$item]["CrowdStrike"] = $true }
foreach ($item in $nessus_hosts) { $allItems[$item]["Nessus"] = $true }

# Prepare an array to hold the output
$output = @()

# Check each item and report if it's missing from any array
$progress = 0
$totalItems = $allItems.Keys.Count

foreach ($item in $allItems.Keys) {
    if ($ignoreHosts["ALL"] -contains $item) {
        continue
    }
    $missingFrom = @()
    foreach ($array in $allItems[$item].Keys) {
        if ($allItems[$item][$array] -eq $false) {
            if ($ignoreHosts.ContainsKey($array) -and ($ignoreHosts[$array] -contains $item)) {
                continue
            }
            $missingFrom += $array
        }
    }
    if ($missingFrom) {
        # Add the item and the missing arrays to the output array
        if ($allItems[$item]["AD"]) {
            $lastLogon = (Get-ADComputer -Property * -Identity $item).LastLogonDate
        } else {
            $lastLogon = ""
        }
        $output += New-Object PSObject -Property @{
            "Item" = $item
            "Last Login" = $lastLogon
            "Missing From" = $missingFrom -join ', '
        }
    }
    # Update the progress bar
    $progress++
    Write-Progress -Activity "Processing Items" -Status "$progress of $totalItems processed" -PercentComplete ($progress / $totalItems * 100)
}

# Export the data as CSV
$date = Get-Date -Format 'yyyyMMddTHHmmss'
$filename = "Compare-Inventory/missing_hosts_$date.csv"
$output | Sort-Object -Property Item | Select-Object -Property Item, 'Last Login', 'Missing From' | Export-Csv -Path $filename -NoTypeInformation
Write-Output "The results have been saved to '$filename'"