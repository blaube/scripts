# Created by Brian Laube 2017

param (
  [Parameter(Mandatory=$true)][string]$csv
)

Import-Module ActiveDirectory

$computers = Import-Csv -Path $csv

foreach ($computer in $computers) {
  $c = $($computer.Name)
  Write-Output $c
  Remove-ADComputer -Identity $c -Confirm:$False
}