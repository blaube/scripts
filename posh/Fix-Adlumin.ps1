# Created by Brian Laube 2024
# Description: Fix Adlumin by copying the required files and running the updater

$dll = Test-Path "C:\Program Files (x86)\Sentry\SA\AdluminTools.dll"
$exe = Test-Path "C:\Program Files (x86)\Sentry\SA\AdluminUpdater.exe"

if (-not $dll) {
    echo "Copy dll"
    cp "\\fs.domain.com\Adlumin\AdluminTools.dll" "C:\Program Files (x86)\Sentry\SA\AdluminTools.dll"
}

if (-not $exe) {
    echo "Copy exe"
    cp "\\fs.domain.com\Adlumin\AdluminUpdater.exe" "C:\Program Files (x86)\Sentry\SA\AdluminUpdater.exe"
}

echo "Running updater..."
& "C:\Program Files (x86)\Sentry\SA\AdluminUpdater.exe"