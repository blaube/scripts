# Created by Brian Laube 2024
# Description: Remove temporary files from the system

$paths=@(
    "$env:TEMP"
    "$env:windir\SoftwareDistribution\Download"
)

$preSize = 0
$postSize = 0

function writelog([string]$message) {
    # Get the current date and time
    $datetime = Get-Date -Format "yyyyMMddTHHmmss"
    
    # Print the message to the console
    Write-Host "$datetime $message"
    
    # Append the message to a log file
    if(!(Test-Path -Path "$env:SystemDrive\scriptlogs")) {
        New-Item -ItemType Directory -Force -Path "$env:SystemDrive\scriptlogs"
    }
    $logFileName = (Split-Path $MyInvocation.PSCommandPath -Leaf) -replace '.ps1$', '.txt'
    "$datetime $message" | Out-File -Append "$env:SystemDrive\scriptlogs\$logFileName"
}

function Get-FolderSize($path) {
    if (Test-Path $path) {
        return (Get-ChildItem -Path $path -Recurse | Measure-Object -Property Length -Sum -ErrorAction SilentlyContinue).Sum / 1GB
    } else {
        Write-Output "Path $path does not exist."
        return 0
    }
}

function Remove-Folder($path) {
    if (Test-Path $path) {
        Get-ChildItem -Path $path -Recurse | Remove-Item -Force -Recurse -ErrorAction SilentlyContinue
    } else {
        Write-Output "Path $path does not exist."
    }
}


foreach($path in $paths){
    $preSize += Get-FolderSize -path $path
    Remove-Folder -path $path
    $postSize += Get-FolderSize -path $path
}

writelog("$datetime Version $ver")
writelog("Total size of folders before removal: $preSize GB")
writelog("Total size of folders after removal: $postSize GB")