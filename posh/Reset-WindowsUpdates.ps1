# Created by Brian Laube 2024
# Description: Reset Windows Updates

Write-Progress -Activity "Resetting Windows Updates" -Status "Stopping services" -PercentComplete 0

net stop wuauserv
net stop cryptSvc
net stop bits
net stop msiserver

$seconds = 10
for ($i = 1; $i -le $seconds; $i++) {
    $percentComplete = ($i / $seconds) * 100
    Write-Progress -Activity "Resetting Windows Updates" -Status "Sleeping" -PercentComplete $percentComplete
    Start-Sleep -Seconds 1
}

Write-Progress -Activity "Resetting Windows Updates" -Status "Removing directories" -PercentComplete 50

$dirs = @(
    "C:\Windows\SoftwareDistribution"
    "C:\Windows\System32\catroot2"
)

$totalDirs = $dirs.Count
$completedDirs = 0

foreach ($dir in $dirs) {
    if (Test-Path -Path $dir) {
        Remove-Item -Path $dir -Force -Recurse
        Start-Sleep -Seconds 1
    }
    Move-Item -Path $dir -Destination "$dir.old" -Force
    $completedDirs++
    $percentComplete = ($completedDirs / $totalDirs) * 100
    Write-Progress -Activity "Resetting Windows Updates" -Status "Removing directories" -PercentComplete $percentComplete
}

$seconds = 10
for ($i = 1; $i -le $seconds; $i++) {
    $percentComplete = ($i / $seconds) * 100
    Write-Progress -Activity "Resetting Windows Updates" -Status "Sleeping" -PercentComplete $percentComplete
    Start-Sleep -Seconds 1
}

Write-Progress -Activity "Resetting Windows Updates" -Status "Starting services" -PercentComplete 75

net start wuauserv
net start cryptSvc
net start bits
net start msiserver

Write-Progress -Activity "Resetting Windows Updates" -Status "Updating Windows" -PercentComplete 100

wuauclt /updatenow