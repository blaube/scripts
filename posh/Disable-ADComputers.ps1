# Created by Brian Laube 2024
# Description: Disable a list of computers in Active Directory

# Import the text file containing computer names
$computerNames = Get-Content -Path "Disable-ADComputers.txt"

# Disable each computer in Active Directory
foreach ($computerName in $computerNames) {
    # Disable the computer
    Write-Host $computerName
    Set-ADComputer -Identity $computerName -Enabled $false
}