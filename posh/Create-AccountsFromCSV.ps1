# Created by Brian Laube 2018
# Description: Add staff AD and Google accounts from CSV
# Requires module NTFSSecurity
# Install-Module -Name NTFSSecurity

param (
  [Parameter(Mandatory=$true)][string]$csv,
  [Parameter(Mandatory=$false)][switch]$create
)

Import-Module ActiveDirectory
Import-Module NTFSSecurity

$users = Import-Csv -Path $csv

foreach ($user in $users) {
  $lastname = $user.SN
  $lastnameSimple = $user.SN -replace " Jr","" -replace " IV","" -replace " III","" -replace " II",""
  $firstname = $user.GivenName
  $account = $($user.GivenName.SubString(0,1) + $lastnameSimple).ToLower()
  $account = $account -replace '[^a-zA-Z0-9]', ''
  $email = "$account@domain.com"
  $path = $user.path
  $googlepath = $user.googlepath
  $password = $user.password
  
  # Make sure user SAMaccountname is unique
  if (Get-ADUser -LDAPFilter "(SAMAccountName=$account)"){
      Write-Host -foregroundcolor Yellow -BackgroundColor Black "$account already exists! Skipping."
  } else {
    if ($create){
      Write-Host -foregroundcolor Yellow -BackgroundColor Black "Creating AD account: $account"
      $folder = "\\path\to\folder\$account"
      New-Item -ItemType directory -Path $folder | Out-Null
      New-ADUser -samaccountname $account `
      -givenname $firstname `
      -surname $lastname `
      -userprincipalname "$account@domain.local" `
      -name "$lastname, $firstname" `
      -displayname "$lastname, $firstname" `
      -emailaddress "$email" `
      -accountpassword (convertto-securestring $password -asplaintext -force) `
      -enabled $true `
      -homedrive "L" `
      -homedirectory $folder `
      -Path $path
      echo "$lastname"
      Write-Host "Creating Google account: $email"
      gam.exe create user $email firstname $firstname lastname $lastname password $password org $googlepath
      Write-Host "Editing User Folder Permissions: $folder"
      Get-Item $folder | Add-NTFSAccess -Account $account -AccessRights FullControl
    }else{
      Write-Host -foregroundcolor Yellow -BackgroundColor Black "$account ($email) does not exist."
    }
  }
}