# Created by Brian Laube 2023
# Description: Export all computers in Active Directory that haven't been logged into for the past N days to a CSV file

$ignoreHosts=@(
    # "HOSTNAME",
    # "HOSTNAME2"
)

# Import the Active Directory module
Import-Module ActiveDirectory

# Get the date N days ago
$days = -90
$NDaysAgo = (Get-Date).AddDays($days)

# Get today's date
$dateTime = Get-Date -Format "yyyyMMddTHHmmss"

# Get all computers
$computers = Get-ADComputer -Filter * -Property *

# Filter out computers that haven't been logged into for the past N days
$oldComputers = $computers | Where-Object { ($_.LastLogonDate -lt $NDaysAgo) -and ($ignoreHosts -notcontains $_.Name) -and ($_.Enabled -eq $true) }

# Sort the old computers by name
$oldComputers = $oldComputers | Sort-Object Name

# Export the old computers to a CSV file
$oldComputers | Select-Object Name, LastLogonDate | Export-Csv -Path ".\export\Export-OldADComputers_$datetime$days.csv" -NoTypeInformation