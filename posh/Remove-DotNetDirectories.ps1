# Created by Brian Laube 2024
# Description: Remove traces old versions of .NET

# SET MINIMUM VERSION
[version]$minVersion = "8.0.8"

# CLEANUP OLD FOLDERS
$netPaths = Get-ChildItem -Path "C:\Program Files\dotnet\shared\Microsoft.NetCore.App\*", "C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App\*", "C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App\*", "C:\Program Files\dotnet\swidtag\*", "C:\Program Files\dotnet\host\fxr\*" -ErrorAction SilentlyContinue | Select-Object -ExpandProperty FullName

foreach ($netPath in $netPaths) {
    $fileName = Split-Path -Leaf $netPath

    [version]$folderVersion = $fileName -replace "Microsoft Windows Desktop Runtime - " -replace " \(x64\).swidtag"
    Write-Host $folderVersion

    if ([version]$folderVersion -lt [version]$minVersion) {
        Write-Host "Remove $netPath as $folderVersion is less-than $minVersion"`
        try {
            Remove-Item $netPath -Recurse -Force
        } catch {
            Write-Host "$_.Exception.Message"
        }
    } else {
        Write-Host "Keep $netPath as $folderVersion is greater-than or equal to $minVersion"
    }
}