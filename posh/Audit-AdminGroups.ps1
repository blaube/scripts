# Created by Brian Laube 2018
# Description: Export CSV of privileged group membership

Import-Module ActiveDirectory

$DirDate = Get-Date -format yyyyMMddTHHmm
$path = Get-Location
$scriptName = $MyInvocation.MyCommand.Name
$outFile = "$path\$scriptName-$DirDate.csv"

$Groups = @("DHCP Administrators", "DNSAdmins", "WSUS Administrators",
"Domain Admins", "Group Policy Creator Owners", "Key Admins",
"Protected Users", "Enterprise Admins", "Enterprise Key Admins",
"Schema Admins")

ForEach ($Group in $Groups) {
    $Members += Get-ADGroupMember -Identity $Group |
    ForEach-Object {Get-ADUser -Identity $_.distinguishedname -Properties displayname, samaccountname, lastlogondate |
    Select-Object @{Name='group';Expression={$Group}}, displayname, samaccountname, lastlogondate, enabled}
}

$Members | Export-Csv -Path $outFile -NoTypeInformation
