# Created by Brian Laube 2024
# Description: Remove old versions of Google Chrome

# SET MINIMUM VERSION
[version]$minVersion = "129.0.6668.59"

# CLEANUP OLD FOLDERS
$versionPaths = Get-ChildItem -Path "C:\Program Files\Google\Chrome\Application" -ErrorAction SilentlyContinue | Select-Object -ExpandProperty FullName

foreach ($versionPath in $versionPaths) {
    $fileName = Split-Path -Leaf $versionPath
    
    try {
        [version]$folderVersion = $fileName # -replace "Google.Chrome" -replace " \(x64\).swidtag \.xml \.exe"
    } catch {
        # Write-Host "Error converting $fileName to version. Skipping..."
        continue
    }
    Write-Host $folderVersion

    if ([version]$folderVersion -lt [version]$minVersion) {
        Write-Host "Remove $versionPath as $folderVersion is less-than $minVersion"
        try {
            Remove-Item $versionPath -Recurse -Force
        } catch {
            Write-Host "$_.Exception.Message"
        }
    } else {
        Write-Host "Keep $versionPath as $folderVersion is greater-than or equal to $minVersion"
    }
}