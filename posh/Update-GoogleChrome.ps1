# Created by Brian Laube 2024
# Description: Update Google Chrome

# Update Google Chrome using GoogleUpdate.exe
$googleUpdatePath = "C:\Program Files (x86)\Google\Update\GoogleUpdate.exe"
if (Test-Path $googleUpdatePath) {
    Start-Process -FilePath $googleUpdatePath -ArgumentList "/ua /installsource scheduler" -Wait
    Write-Host "Google Chrome update initiated using GoogleUpdate.exe."
} else {
    Write-Host "GoogleUpdate.exe not found at $googleUpdatePath."
}