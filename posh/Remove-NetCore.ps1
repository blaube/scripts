# Created by Brian Laube 2024
# Description: Remove old versions of .NET

$paths=@(
    "C:\Program Files\dotnet\shared\Microsoft.NETCore.App"
    "C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App"
    "C:\Program Files\dotnet\swidtag"
    "C:\Program Files (x86)\dotnet\shared\Microsoft.NETCore.App"
    "C:\Program Files (x86)\dotnet\shared\Microsoft.WindowsDesktop.App"
    "C:\Program Files (x86)\dotnet\swidtag"
)

$dotnetVersions=@(
    "3.1.32*"
    "5.0.17*"
)

function writelog([string]$message) {
    # Get the current date and time
    $datetime = Get-Date -Format "yyyyMMddTHHmmss"
    
    # Print the message to the console
    Write-Host "$datetime $message"
    
    # Append the message to a log file
    if(!(Test-Path -Path "$env:SystemDrive\scriptlogs")) {
        New-Item -ItemType Directory -Force -Path "$env:SystemDrive\scriptlogs"
    }
    $logFileName = (Split-Path $MyInvocation.PSCommandPath -Leaf) -replace '.ps1$', '.txt'
    "$datetime $message" | Out-File -Append "$env:SystemDrive\scriptlogs\$logFileName"
}

foreach ($dotnetVersion in $dotnetVersions){
    foreach ($path in $paths) {
        $dotnetPath = "$path\*$dotnetVersion"
        if (Test-Path -Path $dotnetPath) {
            Remove-Item -Path $dotnetPath -Recurse -Force
            writelog(".NET Core Runtime $dotnetVersion has been uninstalled from $path.")
        } else {
            writelog(".NET Core Runtime $dotnetVersion directory not found in $path. No action taken.")
        }
    }
}