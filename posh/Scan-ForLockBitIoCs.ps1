# Written by Brian Laube & John Borrero 20211207
# Specify multiple paths within quotes, "C:\ D:\ E:\"

param($paths="C:\")
$paths = $paths.split(" ")
foreach ($path in $paths){
    echo "Searching $path for lockbit & onion files..."
    Get-ChildItem -recurse -path $path -Include "*lockbit*", "*onion*"
    echo "Done"
}

echo "Checking for safeboot.bat..."
test-path -path "C:\safeboot.bat"

echo "Checking Registry..."
$regkeys = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Run\{356A6003-1616-ACEA-19AA-194F316EB255} HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run\XO1XADpO01 HKCU:\SOFTWARE\LockBit HKCU:\SOFTWARE\LockBit\full HKCU:\SOFTWARE\LockBit\Public"
$regkeys = $regkeys.split(" ")
foreach ($regkey in $regkeys) {
    echo $regkey
    test-path $regkey
}
