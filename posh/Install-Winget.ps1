# Created by Brian Laube 2024
# Description: Install the latest version of the Windows Package Manager (winget-cli) using the MSIX bundle

param (
    [switch]$Online
)

function writelog([string]$message) {
    # Get the current date and time
    $datetime = Get-Date -Format "yyyyMMddTHHmmss"
    
    # Print the message to the console
    Write-Host "$datetime $message"
    
    # Append the message to a log file
    if(!(Test-Path -Path "$env:SystemDrive\scriptlogs")) {
        New-Item -ItemType Directory -Force -Path "$env:SystemDrive\scriptlogs"
    }
    $logFileName = (Split-Path $MyInvocation.PSCommandPath -Leaf) -replace '.ps1$', '.txt'
    "$datetime $message" | Out-File -Append "$env:SystemDrive\scriptlogs\$logFileName"
}

$fileName = "Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle"
$localPath = "C:\downloads"
$remotePath = "\\fs.domain.com\SOFTWARE\Microsoft_Winget"

writelog("Starting installation")

if ($Online) {
    writelog("ONLINE FILE")
    $apiUrl = "https://api.github.com/repos/microsoft/winget-cli/releases/latest"
    $response = Invoke-RestMethod -Uri $apiUrl
    $asset = $response.assets | Where-Object { $_.browser_download_url -like "*msixbundle*" }
    $downloadUrl = $asset.browser_download_url
    $downloadUrl
    Invoke-WebRequest -Uri $downloadUrl -OutFile "$localPath\$fileName"
} else {
    writelog("LOCAL FILE $remotePath\$fileName")
    try {
        Copy-Item -Path "$remotePath\$fileName" -Destination $localPath -ErrorAction Stop
        writelog("File copied successfully")
    } catch {
        $errorMessage = $_.Exception.Message
        writelog("Failed to copy file. Error: $errorMessage")
    }
}

writelog("Installing")
Add-AppXPackage -Path "$localPath\$fileName"

writelog("Done")