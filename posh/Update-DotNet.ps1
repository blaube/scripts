# Created by Brian Laube 2024
# Description: Update the .NET SDK to the latest version and remove old versions

# SET MINIMUM VERSION
[version]$minVersion = "8.0.8"

# INSTALL NEW VERSION
winget install Microsoft.DotNet.SDK.$($minVersion.Major)

# UNINSTALL OLD VERSIONS (UPDATE THIS LIST AS NEEDED. WILL AUTOMATE IN THE FUTURE)
winget uninstall Microsoft.DotNet.SDK.6
winget uninstall Microsoft.NetCore.6
winget uninstall Microsoft.DotNet.SDK.6.0*
winget uninstall Microsoft.NetCore.6.0*
winget uninstall Microsoft.DotNet.SDK.7
winget uninstall Microsoft.AspNetCore.App\7.0
winget uninstall Microsoft.NetCore.App\7.0
winget uninstall Microsoft.NetCore.7
winget uninstall Microsoft.ASPNetCore.7
winget uninstall Microsoft.WindowsDesktop.App.7
winget uninstall Microsoft.WindowsDesktop.App.6
winget uninstall Microsoft.NetCore.App.8.0.6
winget uninstall Microsoft.Dotnet.Desktopruntime.8.0.6
winget uninstall Microsoft.Dotnet.AspNetcore.8.0.6


# CLEANUP OLD FOLDERS
$netPaths = Get-ChildItem -Path "C:\Program Files\dotnet\shared\Microsoft.NetCore.App\*", "C:\Program Files\dotnet\shared\Microsoft.AspNetCore.App\*", "C:\Program Files\dotnet\shared\Microsoft.WindowsDesktop.App\*", "C:\Program Files\dotnet\swidtag\*", "C:\Program Files\dotnet\host\fxr\*" -ErrorAction SilentlyContinue | Select-Object -ExpandProperty FullName

foreach ($netPath in $netPaths) {
    $fileName = Split-Path -Leaf $netPath

    [version]$folderVersion = $fileName -replace "Microsoft Windows Desktop Runtime - " -replace " \(x64\).swidtag"
    Write-Host $folderVersion

    if ([version]$folderVersion -lt [version]$minVersion) {
        Write-Host "Remove $netPath as $folderVersion is less-than $minVersion"`
        try {
            Remove-Item $netPath -Recurse -Force
        } catch {
            Write-Host "$_.Exception.Message"
        }
    } else {
        Write-Host "Keep $netPath as $folderVersion is greater-than or equal to $minVersion"
    }
}