# Created by Brian Laube 2024
# Description: Compare two SonicWALL firewall configuration files and output a CSV of the differences

param(
    [Parameter(Mandatory=$true)]
    [ValidateScript({Test-Path $_ -PathType 'Leaf'})]
    [string]$currentConfigFile,

    [Parameter(Mandatory=$true)]
    [ValidateScript({Test-Path $_ -PathType 'Leaf'})]
    [string]$oldConfigFile,

    [Parameter()]
    [int]$Lines,

    [Parameter()]
    [switch]$ClearCache,

    [Parameter()]
    [switch]$Test
)

# $ignoredKeywords=@(
#     "shared-secret"
#     "shared-key"
#     "password"
#     "passphrase"
#     "smtp-pass"
#     "pop-pass"
#     "uuid"
#     "checksum"
# )

$dateTime = Get-Date -Format "yyyyMMddTHHmmss"
$outFile = "$(Split-Path -Parent $currentConfigFile)\$(Split-Path -LeafBase $currentConfigFile)_vs_$(Split-Path -LeafBase $oldConfigFile)_$dateTime.csv"
$tempPath = "C:\Windows\Temp"
$oldUniquePath = "$tempPath\$(Split-Path -LeafBase $oldConfigFile)_uniq.txt"
$currentUniquePath = "$tempPath\$(Split-Path -LeafBase $currentConfigFile)_uniq.txt"
$regexIgnoreStrings = '(?:\s*priority( manual)? \d+\s*|\s*uuid [0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}|\s*checksum \d+|\s*\S*key 6,\S*|\s*\S*password 6,\S*|\s*\S*-pass 6,\S*|\s*\S*secret 6,\S*)'
$regexCondenseStrings = '(?:(source|destination) address (group|name|service))|(service|log|network|object|system|users|voip|vpn|wireless) (name|group)|from\s+\S+\s+to\s+\S+'
$regexRemoveStrings = '(?:\s*action (allow|deny|discard)|\s*priority( manual)? \d+|\s*_|no\s+|\s*>)'
$oldConfigConv = @()
$currentConfigConv = @()

function Clear-Cache() {
    Remove-Item -Path $oldUniquePath -Force
    Remove-Item -Path $currentUniquePath -Force
    Remove-Item -Path "$tempPath\$(Split-Path -LeafBase $oldConfigFile)_conv.txt" -Force
    Remove-Item -Path "$tempPath\$(Split-Path -LeafBase $currentConfigFile)_conv.txt" -Force
}

function Get-ConfigConv($configFile) {
    $fileName = "$(Split-Path -LeafBase $configFile)_conv.txt"
    # Check if converted syntax file exist
    if (!(Test-Path "$tempPath\$fileName")){
        $a = Convert-Syntax $configFile
        $a | Out-File -FilePath "$tempPath\$fileName"
    } else {
        $a = Get-Content "$tempPath\$fileName"
    }
    return $a
}

function Convert-Syntax($filePath) {
    $lines = Get-Content $filePath #-TotalCount 10000
    $lineCount = 0
    $totalLines = $lines.Count
    $fileArray = @()
    $lastSection = @()
    $fileName = Split-Path -Leaf $filePath

    foreach ($line in $lines) {
        $lineCount++
        Write-Progress -Activity "Converting $fileName" -Status "$lineCount of $totalLines" -PercentComplete (($lineCount / $totalLines) * 100)

        # Ignore blank lines
        if ([string]::IsNullOrWhiteSpace($line)) {
            continue
        }

        # # Check if line contains any of the ignored keywords
        # foreach ($keyword in $ignoredKeywords) {
        #     if ($line -match $keyword) {
        #         # Remove all text to the right of the keyword
        #         $line = $line -replace "$keyword.*", $keyword
        #         break
        #     }
        # }

        $indentLevel = ($line -replace "(^ *).*",'$1').Length / 4
        if ($lastSection.Count -le $indentLevel) {
            $lastSection += ,@() * ($indentLevel - $lastSection.Count + 1)
        }
        $lastSection[$indentLevel] = $line.Trim()

        $lastSectionText = ""
        $lastSectionText = ($lastSection[0..$indentLevel] -join " > ")

        $fileArray += $lastSectionText
    }

    return $fileArray
}

function Compare-Strings($string1, $string2) {
 
    if ($string1 -match "no\s+" -xor $string2 -match "no\s+") {
        $notes = "INVERTED "
    }

    # Replace regex with spaces
    # $string1 = $($string1 -replace $regex, " ").Trim()
    # $string2 = $($string2 -replace $regex, " ").Trim()

    # Remove regex patterns
    $string1 = $($string1 -replace $regexRemoveStrings, $null).Trim()
    $string2 = $($string2 -replace $regexRemoveStrings, $null).Trim()

    # Replace spaces within regex with nothing
    $string1 = $string1 -replace $regexCondenseStrings, { $_.Value -replace " " }
    $string2 = $string2 -replace $regexCondenseStrings, { $_.Value -replace " " }

    # Tokenize the strings by white spaces
    $string1Tokens = $($string1 -split '\s+')
    $string2Tokens = $($string2 -split '\s+')

    if ($string1Tokens[0] -ne $string2Tokens[0]) {
        return @{ Percent = 0; Notes = $null }
    }
    
    # Calculate the number of matching tokens and total tokens
    $matchingTokens = ($string2Tokens | Where-Object { $string1Tokens -like "*$_*" }).Count
    $matchingTokens2 = ($string1Tokens | Where-Object { $string2Tokens -like "*$_*" }).Count
    #$matchingTokens = (Compare-Object -ReferenceObject $string1Tokens -DifferenceObject $string2Tokens -IncludeEqual | Where-Object { $_.SideIndicator -eq '==' }).Count
    $totalTokens = ($string1Tokens.Count + $string2Tokens.Count) / 2
    $totalMatch = ($matchingTokens + $matchingTokens2) / 2

    # Calculate the percentage of matching tokens
    $matchingPercentage = ($totalMatch / $totalTokens) * 100
    
    return @{ Percent = [Math]::Round($matchingPercentage); Notes = $notes }
}

function Get-Notes($line, $prefix = $null, $default = $null) {
    $line = $line -replace "^no\s+", $null
    $notes = $null
    switch -regex ($line) {
        "^firmware-version" {
            return "$($prefix)firmware version"
         }
         "^rom-version" {
             return "$($prefix)rom version"
         }
         "^system-time" {
             return "$($prefix)system time"
         }
         "^system-uptime" {
             return "$($prefix)system uptime"
         }
         "^last-modified-by" {
             return "$($prefix)last modified by user/date/time"
         }
         "^time" {
             return "$($prefix)date/time"
         }
        "^address-object" {
           $notes += "$($prefix)address-object, "
        }
        "^address-group" {
           $notes += "$($prefix)address-group, "
        }
        "^service-object" {
           $notes += "$($prefix)service-object, "
        }
        "^service-group" {
           $notes += "$($prefix)service-group, "
        }
        "^web-category" {
           $notes += "$($prefix)web-category, "
        }
        "^content-filter" {
           $notes += "$($prefix)content-filter, "
        }
        "^dbg" {
           $notes += "$($prefix)debug settings, "
        }
        "^diag" {
           $notes += "$($prefix)diagnostics technical support report, "
        }
        "^packet-monitor" {
           $notes += "$($prefix)packet monitor configuration, "
        }
        "service name BGP" {
            $notes += "No longer using BGP, "
         }
    }
    
    return "$default$notes".TrimEnd(", ")
}

function Merge-Configs($Current, $Old, $lineLimit) {
    $mergedConfigs = @()
    $currentMatchOld = $null
    $currentMatchNew = $null
    $currentMatchPercent = 0
    $specialMatches = 0

    if (-not $lineLimit) {
        $lineLimit = $Current.Count
    }

    $progressCounter = 0
    foreach ($newLine in $Current) {
        if ($progressCounter -ge $lineLimit) {
            break
        }
        
        $progressCounter++
        Write-Progress -Activity "Comparing similar lines ($specialMatches)($($Current.Count) vs $($Old.Count))" -Status "$progressCounter of $lineLimit completed" -PercentComplete (($progressCounter / $lineLimit) * 100)

        foreach ($oldLine in $Old) {
            # Speed things up
            if ($specialMatches -lt 5){
                if (($oldLine -match "^firmware-version") -and ($newLine -match "^firmware-version")) {
                    $currentMatchPercent = 100
                    $currentMatchOld = $oldLine
                    $currentMatchNew = $newLine
                    $specialMatches++
                    break
                }
                if (($oldLine -match "^rom-version") -and ($newLine -match "^rom-version")) {
                    $currentMatchPercent = 100
                    $currentMatchOld = $oldLine
                    $currentMatchNew = $newLine
                    $specialMatches++
                    break
                }
                if (($oldLine -match "^system-time") -and ($newLine -match "^system-time")) {
                    $currentMatchPercent = 100
                    $currentMatchOld = $oldLine
                    $currentMatchNew = $newLine
                    $specialMatches++
                    break
                }
                if (($oldLine -match "^system-uptime") -and ($newLine -match "^system-uptime")) {
                    $currentMatchPercent = 100
                    $currentMatchOld = $oldLine
                    $currentMatchNew = $newLine
                    $specialMatches++
                    break
                }
                if (($oldLine -match "^last-modified-by") -and ($newLine -match "^last-modified-by")) {
                    $currentMatchPercent = 100
                    $currentMatchOld = $oldLine
                    $currentMatchNew = $newLine
                    $specialMatches++
                    break
                }
            }

            # Use Compare-String to get the matching percentage
            $result = Compare-Strings $oldLine $newLine
            
            if ($result.Percent -gt $currentMatchPercent) {
                $currentMatchPercent = $result.Percent
                $currentMatchOld = $oldLine
                $currentMatchNew = $newLine
                $prefix = $result.Notes
            }
            if ($currentMatchPercent -eq 100) {
                # remove the line from future processing?
                # $Old = $Old | Where-Object { $_ -ne $currentMatchOld }
                $currentMatchPercent = $result.Percent
                $currentMatchOld = $oldLine
                $currentMatchNew = $newLine
                $prefix = $result.Notes
                break
            }
        }
        if (($currentMatchPercent -ge 85)) {  #-and ($currentMatchPercent -ne 100)) {
            $obj = New-Object PSObject -Property @{
                'Similarity' = $currentMatchPercent
                'OldConfig' = $currentMatchOld
                'CurrentConfig' = $currentMatchNew
                'Notes' = "$prefix$(Get-Notes $currentMatchOld 'Old ' 'Format Changed, ') $(Get-Notes $currentMatchNew '/ Current ')"
            } | Select-Object Similarity, OldConfig, CurrentConfig, Notes
            $mergedConfigs += $obj
        }
        $currentMatchOld = $null
        $currentMatchNew = $null
        $currentMatchPercent = 0

    }

    # Go through each line in $Old. If the line does not exist in $mergedConfigs under "OldConfig", add it.
    $lineCount = $Old.Count
    $progressCounter = 0
    foreach ($line in $Old) {
        # if ($Lines) {
        #     break
        # }
        if ($progressCounter -ge $Lines) {
            break
        }
        $progressCounter++
        Write-Progress -Activity "Adding non-similar lines from Old Config" -Status "$progressCounter of $lineCount completed" -PercentComplete (($progressCounter / $lineCount) * 100)
        if ($mergedConfigs.OldConfig -notcontains $line) {
            $mergedConfigs += New-Object PSObject -Property @{
                'Similarity' = $null
                'OldConfig' = $line
                'CurrentConfig' = $null
                'Notes' = Get-Notes $line 'Old '
            } | Select-Object Similarity, OldConfig, CurrentConfig, Notes
        } else {
            continue
        }
    }
    # Do the same thing for $Current
    $lineCount = $Current.Count
    $progressCounter = 0
    foreach ($line in $Current) {
        if ($Lines) {
            break
        }
        $progressCounter++
        Write-Progress -Activity "Adding non-similar lines from Current Config" -Status "$progressCounter of $lineCount completed" -PercentComplete (($progressCounter / $lineCount) * 100)
        if ($mergedConfigs.CurrentConfig -notcontains $line) {
            $mergedConfigs += New-Object PSObject -Property @{
                'Similarity' = $null
                'OldConfig' = $null
                'CurrentConfig' = $line
                'Notes' = Get-Notes $line 'Current '
            } | Select-Object Similarity, OldConfig, CurrentConfig, Notes
        } else {
            continue
        }
    }

    return $mergedConfigs

}

function Get-UniqueConfigs($Current = $currentConfigConv, $Old = $oldConfigConv) {
    $oldUnique = @()
    $currentUnique = @()

    $Old2 = $($Old -replace $regexIgnoreStrings, $null).Trim()
    $Current2 = $($Current -replace $regexIgnoreStrings, $null).Trim()


    if (Test-Path $oldUniquePath) {
        $oldUnique = Get-Content $oldUniquePath
    } else {
        $lineCount = 0
        $totalLines = $Old2.Count
        foreach ($line in $Old2) {
            $lineCount++
            Write-Progress -Activity "Processing Old Config ($($oldUnique.Count))" -Status "$lineCount of $totalLines" -PercentComplete (($lineCount / $totalLines) * 100)

            if ($Current2 -notcontains $line) {
            #if ($Current -notcontains $line) {
                $oldUnique += $Old[$lineCount -1] #$line
            } else {
                if ($line -match "^(system-time|rom-version|system-time|system-uptime|last-modified-by)") {
                    $oldUnique += $Old[$lineCount -1] #$line
                }
                continue
            }
        }
        $oldUnique | Out-File -FilePath $oldUniquePath
    }

    if (Test-Path $currentUniquePath) {
        $currentUnique = Get-Content $currentUniquePath
    } else {
        $lineCount = 0
        $totalLines = $Current.Count
        foreach ($line in $Current2) {
            $lineCount++
            Write-Progress -Activity "Processing Current Config ($($currentUnique.Count))" -Status "$lineCount of $totalLines" -PercentComplete (($lineCount / $totalLines) * 100)
            
            if ($Old2 -notcontains $line) {
            #if ($Old -notcontains $line) {
                $currentUnique += $Current[$lineCount -1] #$line
            } else {
                if ($line -match "^(system-time|rom-version|system-time|system-uptime|last-modified-by)") {
                    $currentUnique += $Current[$lineCount -1] #$line
                }
                continue
            }
        }
        $currentUnique | Out-File -FilePath $currentUniquePath
    }

    return @{ Current = $currentUnique; Old = $oldUnique }

}

if ($ClearCache) {
    Clear-Cache
}

if ($Test){
    $str1 = 'administration > no enforce-http-host-check'
    $str2 = 'administration > enforce-http-host-check'
    Write-Host $(Compare-Strings  $str1 $str2).Percent
    exit
}

Write-Host "Step 1"
$oldConfigConv = Get-ConfigConv $oldConfigFile
Write-Host "Step 2"
$currentConfigConv = Get-ConfigConv $currentConfigFile
Write-Host "Step 3"
$uniqueData = Get-UniqueConfigs
Write-Host "Step 4"
if ($Lines) {
    $c = Merge-Configs $uniqueData.Current $uniqueData.Old $Lines
} else {
    $c = Merge-Configs $uniqueData.Current $uniqueData.Old
}
Write-Host "Step 5"
$c | Export-Csv -Path $outFile -NoTypeInformation -Append
Write-Host "Saved output to $outFile"