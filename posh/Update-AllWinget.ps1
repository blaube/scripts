# Created by Brian Laube 2024
# Description: Update all winget packages

function writelog([string]$message) {
    # Get the current date and time
    $datetime = Get-Date -Format "yyyyMMddTHHmmss"
    
    # Print the message to the console
    Write-Host "$datetime $message"
    
    # Append the message to a log file
    if(!(Test-Path -Path "$env:SystemDrive\scriptlogs")) {
        New-Item -ItemType Directory -Force -Path "$env:SystemDrive\scriptlogs"
    }
    $logFileName = (Split-Path $MyInvocation.PSCommandPath -Leaf) -replace '.ps1$', '.txt'
    "$datetime $message" | Out-File -Append "$env:SystemDrive\scriptlogs\$logFileName"
}


writelog("Update all winget packages")

# Simple command for now. Expand code if needed.
Write-Host 'Y' | winget upgrade --all --include-unknown --accept-package-agreements --accept-source-agreements

writelog("Done")