# Created by Brian Laube 2023
# Description: Export all users in an OU to a CSV file


# Import the Active Directory module
Import-Module ActiveDirectory

# Define the OU
$ou = "OU=USERS,DC=domain,DC=com"

# Get all AD users in the OU and its sub-OUs
$adUsers = Get-ADUser -Filter * -SearchBase $ou -Properties SamAccountName, DisplayName, Description, DistinguishedName

# Select the required properties and calculate the OU path
$adUsers = $adUsers | Select-Object SamAccountName, DisplayName, Description, @{Name='OU';Expression={$_.DistinguishedName -replace '^.+?,(CN|OU.+)','$1'}}

# Export the users to a CSV file
$adUsers | Export-Csv -Path ".\export\ADUsers.csv" -NoTypeInformation