# Created by Brian Laube 2018
# Description: Power off all computers within specified AD OUs

Import-Module ActiveDirectory

function ShutComputers($OU) {
    $Computers = Get-ADComputer -Filter * -SearchBase "$OU" | Select-Object -ExpandProperty Name
    foreach ($Computer in $Computers) {
        if (Test-Connection -Computername $Computer -BufferSize 16 -Count 1 -Quiet) {
            Write-Host "Shutting down $Computer"
            shutdown -s -f -t 300 -m $Computer -c "Shutting down in 5 minutes!"
        } else {
            Write-Host "Cannot contact $Computer"
        }
    }
}

$OU = "OU=Computers1,DC=domain,DC=local"
ShutComputers $OU

$OU = "OU=Computers2,DC=domain,DC=local"
ShutComputers $OU