# Created by Brian Laube 2024
# Description: Remove old versions of Appx packages

$packages = @(
    @{
        Name = "Microsoft.Print3D"
        minVersion = "6.2408.2027.0"
    },
    @{
        Name = "Microsoft.Microsoft3DViewer"
        minVersion = "7.2009.29132.0"
    },
    @{
        Name = "Microsoft.ScreenSketch"
        minVersion = "11.2302.20.0"
        min10Version = "10.2008.3001.0"
    },
    @{
        Name = "Microsoft.SurfaceHub"
        minVersion = "9999.9999.9999.9999"
    }
)

function writelog([string]$message) {
    # Get the current date and time
    $datetime = Get-Date -Format "yyyyMMddTHHmmss"
    
    # Print the message to the console
    Write-Host "$datetime $message"
    
    # Append the message to a log file
    if(!(Test-Path -Path "$env:SystemDrive\scriptlogs")) {
        New-Item -ItemType Directory -Force -Path "$env:SystemDrive\scriptlogs"
    }
    $logFileName = (Split-Path $MyInvocation.PSCommandPath -Leaf) -replace '.ps1$', '.txt'
    "$datetime $message" | Out-File -Append "$env:SystemDrive\scriptlogs\$logFileName"
}

function Remove-OldDirectories {
    writelog("Remove old directories")
    $appPaths = Get-ChildItem -Path "C:\Program Files\WindowsApps\$packageName*" | Where-Object { $_.FullName -like "*$packageName*" } | Select-Object -ExpandProperty FullName
    
    foreach ($appPath in $appPaths) {
        $fileName = Split-Path -Leaf $appPath
        [version]$folderVersion = $fileName -replace "$($packageName)\.?" -replace "_x86.*" -replace "_x64.*" -replace ".*_"
        #Write-Host $folderVersion

        if ([version]$folderVersion -lt [version]$minVersion) {
            writelog("Remove $appPath as $folderVersion is less-than $minVersion")
            try {
                Remove-Item $appPath -Recurse -Force
            } catch {
                writelog("$_.Exception.Message")
            }
        } else {
            writelog("Keep $appPath as $folderVersion is greater-than or equal to $minVersion")
        }
    }
}

foreach ($package in $packages) {
    $packageName = $package.Name
    $osVersion = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").CurrentBuild
    if ($osVersion -lt 22000 -and $package.ContainsKey('min10Version')) {
        $minVersion = $package.min10Version
        writelog("WINDOWS 10 $minVersion")
    } else {
        $minVersion = $package.minVersion
        writelog("WINDOWS GENERIC $minVersion")
    }

    $installedVersions = Get-AppxPackage -Name "*$packageName*" -AllUsers

    if ($installedVersions) {
        foreach ($installedVersion in $installedVersions){
            $version = $installedVersion.Version

            writelog("Version of $packageName $version")
            writelog("Install location: $($installedVersion.InstallLocation)")

            if ([version]$version -lt [version]$minVersion) {
                $installedVersion | ForEach-Object {
                    writelog("Remove $version")
                    Uninstall-AppxPackage -Package $_.PackageFullName -AllUsers
                }
            } else {
                writelog("Up to date")
            }
        }
    } else {
        writelog("Package $packageName not found.")
    }
    
    Remove-OldDirectories
}