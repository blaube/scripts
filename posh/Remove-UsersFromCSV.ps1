# Created by Brian Laube 2018
# Description: Remove AD and Google account of users listed in CSV

param (
  [Parameter(Mandatory=$true)][string]$csv
)

Import-Module ActiveDirectory

$users = Import-Csv -Path $csv

foreach ($user in $users) {

  $sa = $($user.sa)
  $hd = Get-ADUser -Identity $sa -properties homedirectory | select-object -ExpandProperty HomeDirectory
  $email = $("$sa@domain.com")
  echo "Removing $sa : $hd : $email"
  Remove-ADUser -Identity $sa
  Remove-Item -path $hd
  gam.exe delete user $email

}